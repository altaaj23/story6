from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import home
from .models import Message
from .forms import MessageForm

class UnitTest(TestCase):
    
    def test_unitest_url_is_exist(self): 
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    
    def test_using_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()  
        response = home(request)  
        html = response.content.decode('utf8')  
        self.assertTrue(html.startswith('<html>'))  
        self.assertIn('<title>story 6</title>', html)  
        self.assertTrue(html.endswith('</html>')) 

    def test_unitest_using_home_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_model_create_new_message(self):
        Message.objects.create(message='halo')
        count_content = Message.objects.all().count()
        self.assertEqual(count_content, 1)

    def test_form_correct_name(self):
        response = self.client.post('', data={'message' : 'halo'})
        message = Message.objects.get(pk=1)
        self.assertEqual(str(message), message.message)

    def test_form_validation_for_blank_items(self):
        form = MessageForm(data={'message': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['message'],["This field is required."])

#    when I add these last two tests, the coverage reduce to 54%

    

