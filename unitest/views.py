from django.shortcuts import render, redirect
from django.shortcuts import HttpResponse
from django.urls import reverse
from .models import Message
from .forms import MessageForm

def home(request):
    messages = Message.objects.all().order_by('-time')
    if request.method == 'POST':
        message_form = MessageForm(request.POST)
        if message_form.is_valid():
            message_form.save()
            return redirect('home')
    else:
        message_form = MessageForm()
    
    context = {'messages': messages,'message_form': message_form}

    return render(request, 'home.html', context)