from django import forms
from django.forms import ModelForm
from .models import Message
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

class MessageForm(ModelForm):
    class Meta:
        model = Message
        fields = '__all__'
